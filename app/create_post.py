import json
import boto3
from PIL import Image
from io import BytesIO
import uuid
import base64
import time


boto3.setup_default_session(
    aws_access_key_id="test",
    aws_secret_access_key="test",
    region_name="us-east-1",
)
s3 = boto3.client("s3", endpoint_url="http://localstack:4566")
dynamodb = boto3.resource("dynamodb", endpoint_url="http://localstack:4566")
posts_table = dynamodb.Table("Posts")


def lambda_handler(event, context):
    if "s3" in context:
        s3 = context["s3"]
    if "dynamodb" in context:
        posts_table = context["dynamodb"]
    body = json.loads(event["body"])
    caption = body["caption"]
    image_data = body["image"]
    creator = body["creator"]

    post_id = str(uuid.uuid4())

    image = Image.open(BytesIO(base64.b64decode(image_data)))
    original_format = image.format

    original_image_key = f"original/{post_id}.{original_format.lower()}"
    s3.put_object(
        Bucket="my-bucket",
        Key=original_image_key,
        Body=image_data,
        ContentType=f"image/{original_format.lower()}",
    )

    image = image.convert("RGB")
    image = image.resize((600, 600))
    buffer = BytesIO()
    image.save(buffer, format="JPEG")
    buffer.seek(0)

    converted_image_key = f"converted/{post_id}.jpg"
    s3.put_object(
        Bucket="my-bucket",
        Key=converted_image_key,
        Body=buffer,
        ContentType="image/jpeg",
    )

    post_item = {
        "Id": post_id,
        "Caption": caption,
        "OriginalImage": original_image_key,
        "ConvertedImage": converted_image_key,
        "Creator": creator,
        "CreatedAt": int(time.time()),
        "Comments": [],
    }
    posts_table.put_item(Item=post_item)

    return {"statusCode": 201, "body": json.dumps({"post_id": post_id})}
