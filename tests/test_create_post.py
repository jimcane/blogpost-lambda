import json
import base64

from app.create_post import lambda_handler
from PIL import Image
from io import BytesIO
import base64
from moto import mock_aws


@mock_aws
def test_create_post(test_s3, test_dynamodb):
    # Mock event
    image = Image.new("RGBA", (1000, 1000), color="red")
    buffered = BytesIO()
    image.save(buffered, format="PNG")
    image_str = base64.b64encode(buffered.getvalue()).decode("utf-8")
    event = {
        "body": json.dumps(
            {
                "caption": "Test Caption",
                "image": image_str,
                "creator": "Test Creator",
            }
        )
    }

    response = lambda_handler(event, {"s3": test_s3, "dynamodb": test_dynamodb})

    # Parse response
    response_body = json.loads(response["body"])

    assert response["statusCode"] == 201
    assert "post_id" in response_body

    post_id = response_body["post_id"]

    # Check if the original image was uploaded
    original_image_key = f"original/{post_id}.png"
    original_image = test_s3.get_object(
        Bucket="my-bucket", Key=original_image_key
    )
    assert original_image is not None

    # Check if the converted image was uploaded
    converted_image_key = f"converted/{post_id}.jpg"
    converted_image = test_s3.get_object(
        Bucket="my-bucket", Key=converted_image_key
    )
    assert converted_image is not None

    # Check if the post was added to DynamoDB
    table = test_dynamodb
    response = table.get_item(Key={"Id": post_id})
    item = response.get("Item")
    assert item is not None
    assert item["Caption"] == "Test Caption"
    assert item["Creator"] == "Test Creator"
    assert item["OriginalImage"] == original_image_key
    assert item["ConvertedImage"] == converted_image_key
