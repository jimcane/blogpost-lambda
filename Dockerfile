FROM public.ecr.aws/lambda/python:3.12

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY app/ ${LAMBDA_TASK_ROOT}/

CMD ["create_post.lambda_handler"]
