import boto3


boto3.setup_default_session(
    aws_access_key_id="test",
    aws_secret_access_key="test",
    region_name="us-east-1",
)

s3 = boto3.client(
    "s3",
    endpoint_url="http://localhost:4566",
)
dynamodb = boto3.client(
    "dynamodb",
    endpoint_url="http://localhost:4566",
)

# Create S3 bucket
s3.create_bucket(Bucket="my-bucket")

# Create DynamoDB tables
dynamodb.create_table(
    TableName="Posts",
    KeySchema=[{"AttributeName": "Id", "KeyType": "HASH"}],
    AttributeDefinitions=[{"AttributeName": "Id", "AttributeType": "S"}],
    ProvisionedThroughput={"ReadCapacityUnits": 5, "WriteCapacityUnits": 5},
)

dynamodb.create_table(
    TableName="Comments",
    KeySchema=[{"AttributeName": "Id", "KeyType": "HASH"}],
    AttributeDefinitions=[{"AttributeName": "Id", "AttributeType": "S"}],
    ProvisionedThroughput={"ReadCapacityUnits": 5, "WriteCapacityUnits": 5},
)
